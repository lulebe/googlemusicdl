# Requirements #

* Node.js ([Download](http://nodejs.org/download))
* FFMpeg ([Windows/Linux Download](https://www.ffmpeg.org/download.html), use [Homebrew](http://brew.sh/) on OSX, cmd:

```
#!bash

brew install ffmpeg
```

# Installation #
1. navigate to this folder in console
2. type "npm install" to download dependencies

# Using this App #
1. type "node start" in console in this folder
2. log in to your Google Account