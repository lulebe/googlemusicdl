var gpm = require('./play_modified');
var prompt = require('prompt');
var open = require('open');
var get = require('http-get');
var metadata = require('ffmetadata');
var fs = require('fs');
var sanitizeFilename = require('sanitize-filename');
var mkdirp = require('mkdirp');
var p = require('path');


var client = new gpm();


prompt.start();
console.log('Log in to Google to use the App');

var loginschema = {
  properties: {
    email: {
      message: 'Gmail Adress'
    },
    password: {
      hidden: true
    }
  }
};

prompt.get(loginschema, function (err, logindata) {
  if (!logindata) return;
  client.init({email: logindata.email, password: logindata.password}, start);
  
});

function start() {
  prompt.get({properties: {action: {message: 'Single Song (1) or Whole Album (2)'}}}, function (err, action) {
    if (!action) return;
    if (action.action == 1)
      singleSong();
    else
      fullAlbum();
  });
};

function singleSong () {
  console.log('Listen to/Download a Song:');
  prompt.get(['artist','song'], function (err, songdata) {
    if (!songdata) return;

    client.search(songdata.artist + ' ' + songdata.song, 30, function (data) {
      for (var i = 0; i<data.entries.length; i++) {
        var entry = data.entries[i];
        if (entry.type == 1)
          console.log((i)+' - '+entry.track.artist+': '+entry.track.title);
      }
      prompt.get({properties: {song: {message: 'Song Number'}}}, function (err, selection) {
        if (typeof selection === "undefined") return;
        if (selection.song == 'x') 
          return;
        else if (selection.song == 'r')
          start();
        else {
          var track = data.entries[parseInt(selection.song)].track;
          prompt.get({properties: {action: {message: 'Download (1) or Play (2)'}}}, function (err, action) {
            if (action.action == 2) {
              client.getStreamUrl(track.storeId, function (url) {
                open(url);
                start();
              });
            } else
              getDir(null, function (dir) {
                downloadSong(track, dir);
              });
          });
        }
      });
    });

  });
};

var albumDLcount = 0;
function fullAlbum () {
  albumDLcount = 0;
  console.log('Download a whole Album:');
  prompt.get(['artist', 'album'], function (err, albumdata) {
    client.search(albumdata.artist+' '+albumdata.album, 30, function (data) {
      for (var i = 0; i<data.entries.length; i++) {
        var entry = data.entries[i];
        if (entry.type == 3)
          console.log((i)+' - '+entry.album.albumArtist+': '+entry.album.name);
      }
      var props = {properties: {album: {message: 'Album Number (seperate multiple by comma)'}}};
      prompt.get(props, function (err, selection) {
        if (typeof selection === "undefined") return;
        if (selection.album == 'x') 
          return;
        else if (selection.album == 'r')
          start();
        else if (selection.album.indexOf(',') > -1) {
          //multiple albums
          var albumnumbers = selection.album.split(',');
          var albums = [];
          for (var i=0;i<albumnumbers.length;i++)
            albums.push(data.entries[parseInt(albumnumbers[i])].album);
          getSingleAlbum(albums, 0);
        }
        else {
          //one album
          var albumid = data.entries[parseInt(selection.album)].album.albumId;
          client.getAlbum(albumid, function (data) {
            getDir({artist: data.artist, album: data.name}, function (dir) {
              data.tracks.forEach(function (value, index) {
                downloadSong(value, dir, data.tracks.length);
              });
            });
          });
        }
      });
    });
  });
};

function getSingleAlbum (albums, albumindex) {
  albumDLcount = 0;
  client.getAlbum(albums[albumindex].albumId, function (data) {
    var artist = sanitizeFilename(data.artist);
    var name = sanitizeFilename(data.name);
    var dir = p.normalize(artist+'/'+name);
    mkdirp.sync(dir);
    data.tracks.forEach(function (value, index) {
      downloadSong(value, dir, data.tracks.length, function () {
        albumindex++;
        if (albumindex < albums.length)
          getSingleAlbum(albums, albumindex);
        else
          start();
      });
    });
  });
};

function downloadSong (track, dir, albumlength, albumcb) {
  client.getStreamUrl(track.storeId, function (url) {
    var file = track.title+'.mp3';
    if (!albumlength)
      file = track.artist+'_'+file;
    file = sanitizeFilename(file);
    file = dir+'/'+file;
    file = p.normalize(file);
    //if (albumlength != undefined)
    //  file = track.artist+'_'+track.album+'/'+file;
    console.log('downloading...');
    console.log(file);
    var req = get.get(url, file, function (geterr, result) {
      if (geterr) console.log(geterr);
      else {
        console.log('downloaded successfully.');
        console.log('adding metadata...');
        var meta = {
          artist: track.artist,
          album: track.album,
          title: track.title,
          track: track.trackNumber,
          genre: track.genre
        };
        if (fs.existsSync('cover.tmp.jpg')) 
          writeMeta(result.file, meta, albumlength, albumcb);
        else
          var coverreq = get.get(track.albumArtRef[0].url, 'cover.tmp.jpg', function (covererr, covres) {
            if (covererr) { console.log('error loading cover.'); start();}
            else writeMeta(result.file, meta, albumlength, albumcb);
          });
      }
    });
  });
};


function writeMeta (path, meta, albumlength, albumcb) {
  metadata.write(path, meta, ['cover.tmp.jpg'], function (metaerr) {
    if (metaerr) console.log(metaerr);
    else {
      console.log('metadata written.');
    }
    albumDLcount++;
    console.log(albumDLcount+'/'+albumlength);
    if (albumlength === undefined || albumDLcount === albumlength) {
      fs.unlink('cover.tmp.jpg', function (delerr) {
        if (delerr) console.log('error.');
        console.log('DONE!!!');
        console.log('-------');
        if (typeof albumcb !== "undefined")
          albumcb();
        else
          start();
      });
    }
  });
};


function getDir (albumname, callback) {
  console.log(
    'Directory to save in (leave empty to use current or, if loading an album, create accordingly named one)'
  );
  prompt.get(['directory'], function (err, result) {
    if (typeof result === "undefined") return;
    if (result.directory.length > 0) {
      var path = p.normalize(result.directory);
      if (!fs.existsSync(path))
        mkdirp.sync(path)
      callback(path);
    } else if (albumname.artist) {
      albumname.artist = sanitizeFilename(albumname.artist);
      albumname.album = sanitizeFilename(albumname.album);
      var path = p.normalize(albumname.artist+'/'+albumname.album);
      mkdirp.sync(path);
      callback(path);
    } else if (albumname) {
      albumname = sanitizeFilename(albumname);
      mkdirp.sync(albumname);
      callback(albumname);
    } else
      callback('.');
  });
};